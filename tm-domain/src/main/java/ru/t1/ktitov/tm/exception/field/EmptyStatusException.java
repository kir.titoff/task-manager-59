package ru.t1.ktitov.tm.exception.field;

public class EmptyStatusException extends AbstractFieldException {

    public EmptyStatusException() {
        super("Error! Status is empty.");
    }

}
