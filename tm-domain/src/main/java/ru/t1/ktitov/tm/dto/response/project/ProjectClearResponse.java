package ru.t1.ktitov.tm.dto.response.project;

import lombok.Getter;
import lombok.Setter;
import ru.t1.ktitov.tm.dto.response.AbstractResponse;

@Getter
@Setter
public final class ProjectClearResponse extends AbstractResponse {
}
