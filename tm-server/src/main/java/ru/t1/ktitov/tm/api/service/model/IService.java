package ru.t1.ktitov.tm.api.service.model;

import ru.t1.ktitov.tm.api.repository.model.IRepository;
import ru.t1.ktitov.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {
}
