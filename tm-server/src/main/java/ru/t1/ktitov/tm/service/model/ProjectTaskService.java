package ru.t1.ktitov.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ktitov.tm.api.repository.model.IProjectRepository;
import ru.t1.ktitov.tm.api.repository.model.ITaskRepository;
import ru.t1.ktitov.tm.api.service.model.IProjectTaskService;
import ru.t1.ktitov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.ktitov.tm.exception.entity.TaskNotFoundException;
import ru.t1.ktitov.tm.exception.field.EmptyProjectIdException;
import ru.t1.ktitov.tm.exception.field.EmptyTaskIdException;
import ru.t1.ktitov.tm.exception.field.EmptyUserIdException;
import ru.t1.ktitov.tm.model.Project;
import ru.t1.ktitov.tm.model.Task;

import javax.transaction.Transactional;
import java.util.List;

@Service
public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    @Autowired
    private IProjectRepository projectRepository;

    @NotNull
    @Autowired
    private ITaskRepository taskRepository;

    @Override
    @SneakyThrows
    @Transactional
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyTaskIdException();
        if (projectRepository.findOneById(userId, projectId) == null) throw new ProjectNotFoundException();
        @Nullable final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        @Nullable final Project project = projectRepository.findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        task.setProject(project);
        taskRepository.update(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();
        if (taskId == null || taskId.isEmpty()) throw new EmptyTaskIdException();
        if (projectRepository.findOneById(userId, projectId) == null) throw new ProjectNotFoundException();
        @Nullable final Task task = taskRepository.findOneById(userId, taskId);
        if (task == null) throw new TaskNotFoundException();
        @Nullable final Project project = projectRepository.findOneById(userId, projectId);
        if (project == null) throw new ProjectNotFoundException();
        task.setProject(null);
        taskRepository.update(task);
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new EmptyProjectIdException();
        if (projectRepository.findOneById(userId, projectId) == null) throw new ProjectNotFoundException();
        @NotNull final List<Task> tasks = taskRepository.findAllByProjectId(userId, projectId);
        for (final Task task : tasks) taskRepository.removeById(task.getId());
        @Nullable final Project project = projectRepository.findOneById(userId, projectId);
        projectRepository.removeById(userId, projectId);
        return project;
    }

}
