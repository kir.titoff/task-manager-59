package ru.t1.ktitov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ktitov.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.t1.ktitov.tm.api.service.dto.IUserOwnedDtoService;
import ru.t1.ktitov.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.ktitov.tm.enumerated.Sort;
import ru.t1.ktitov.tm.exception.entity.EntityNotFoundException;
import ru.t1.ktitov.tm.exception.field.EmptyIdException;
import ru.t1.ktitov.tm.exception.field.EmptyUserIdException;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@Service
public abstract class AbstractUserOwnedDtoService<M extends AbstractUserOwnedModelDTO, R extends IUserOwnedDtoRepository<M>>
        extends AbstractDtoService<M, R> implements IUserOwnedDtoService<M> {

    @NotNull
    @Autowired
    protected IUserOwnedDtoRepository<M> repository;

    @Nullable
    @SneakyThrows
    @Transactional
    public M add(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (model == null) throw new EntityNotFoundException();
        model.setUserId(userId);
        repository.add(model);
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<M> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return repository.findAll(userId);
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (sort == null) return findAll(userId);
        return repository.findAll(userId, sort);
    }

    @NotNull
    @Override
    @SneakyThrows
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final M model = repository.findOneById(userId, id);
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findOneById(userId, id) != null;
    }

    @Nullable
    @SneakyThrows
    @Transactional
    public M remove(@Nullable final String userId, @Nullable final M model) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (model == null) throw new EntityNotFoundException();
        if (!userId.equals(model.getUserId())) throw new EntityNotFoundException();
        repository.remove(model);
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @Nullable final M model = findOneById(userId, id);
        repository.removeById(userId, id);
        return model;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        repository.clear(userId);
    }

}
