package ru.t1.ktitov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.ktitov.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.ktitov.tm.api.service.dto.ISessionDtoService;
import ru.t1.ktitov.tm.dto.model.SessionDTO;
import ru.t1.ktitov.tm.repository.dto.SessionDtoRepository;

@Service
public final class SessionDtoService extends AbstractUserOwnedDtoService<SessionDTO, ISessionDtoRepository>
        implements ISessionDtoService {

    @NotNull
    @Autowired
    private ISessionDtoRepository repository;

}
